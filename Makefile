#
# makefile for abc tunes
#
### Copyright (C) 2020, Samuel S Chessman
### SPDX-License-Identifier: GPL-3.0-or-later
ABCDIR=abc
MIDDIR=mid
MP3DIR=mp3
PSDIR=ps
PDFDIR=pdf
SRC := $(wildcard $(ABCDIR)/*.abc)
MID := $(patsubst $(ABCDIR)/%.abc, $(MIDDIR)/%.mid, $(SRC))
MP3 := $(patsubst $(ABCDIR)/%.abc, $(MP3DIR)/%.mp3, $(SRC))
PDF := $(patsubst $(ABCDIR)/%.abc, $(PDFDIR)/%.pdf, $(SRC))
PS  := $(patsubst $(ABCDIR)/%.abc, $(PSDIR)/%.ps, $(SRC))

ABC2PS=abcm2ps
ABC2MID=abc2midi
PS2PDF=ps2pdf
MID2WAV=timidity
WAV2MP3=lame

$(MIDDIR)/%.mid : $(ABCDIR)/%.abc
	$(ABC2MID) $^ -quiet -o $@

$(PDFDIR)/%.pdf : $(PSDIR)/%.ps
	-$(PS2PDF) $^ $@

# include guitar chords -F fmt/guitarchords.fmt 
$(PSDIR)/%.ps : $(ABCDIR)/%.abc
	$(ABC2PS) -O $@ -i $^

$(MP3DIR)/%.mp3 : $(MIDDIR)/%.mid
	$(MID2WAV) --quiet=8 -A400 -Ow $^ -o - | $(WAV2MP3) --quiet - $@

.PHONY:	mid pdf ps mp3 clean

# mid mp3
all: ps pdf mp3
mid: $(MID)
mp3: $(MP3)
pdf: $(PDF) $(PS)
ps: $(PS)

clean:
	rm -f $(MID) $(MP3) $(PDF) $(PS)
