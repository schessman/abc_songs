# ABC_songs
GPL3+ ABC sources for various tunes.

Copyrights for music belong to their respective composers.

## License
### Copyright (C) 2020, Samuel S Chessman
### SPDX-License-Identifier: GPL-3.0-or-later
The sources are found at https://gitlab.com/schessman/abc_songs/

## Credits
These files are original works by Sam Chessman based on the abc reference manual and various tutorials.

## Directories

* abc - source files in ABC syntax
* mid - midi file with piano voicing
* mp3 - mp3 of midi file
* pdf - printable/viewable file
* ps  - printable file

## Files

ABC, Midi, MP3, Postscript and Portable Document Format files are provided.

The makefile has rules to build .mid, .mp3, .pdf and .ps files from .abc source files.  It assumes the directory structure above.

* Makefile: instructions to run abcm2ps, abc2midi, timidity, lame
* README.md: this file
* WallsOfTime.\*: Walls of Time by Bill Monroe and Peter Rowan
* WallsOfTimeIntro.\*: Ricky Skaggs Mandolin intro to Walls of Time
* CatfishJohn.\*: Catfish John by Bob McDill and Allen Reynolds
* GreenvilleTrestleHigh.\*: Greenville Trestle High by Jimmy Jett
